libdata-treedumper-renderer-dhtml-perl (0.09-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Apply multi-arch hints. + libdata-treedumper-renderer-dhtml-perl: Add Multi-Arch: foreign.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 20 Nov 2022 02:46:18 +0000

libdata-treedumper-renderer-dhtml-perl (0.09-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 14:11:32 +0100

libdata-treedumper-renderer-dhtml-perl (0.09-2) unstable; urgency=medium

  * Team upload

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/rules: switch order of arguments to dh.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Mark package as source format 3.0 (quilt)
  * Update license paragraphs to commonly used versions
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.4
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Tue, 26 Jun 2018 23:37:25 +0200

libdata-treedumper-renderer-dhtml-perl (0.09-1) unstable; urgency=low

  * Initial Release. (Closes: #526340)

 -- Peter Pentchev <roam@ringlet.net>  Mon, 25 May 2009 15:16:18 +0300
